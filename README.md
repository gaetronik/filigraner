![Build Status](https://gitlab.com/gaetronik/filigraner/badges/master/pipeline.svg)


Une Single File Application pour ajouter un filigrane dans le navigateur.

Disponible sur <https://filigraner.pignouf.fr>
